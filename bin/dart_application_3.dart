//import 'package:dart_application_3/dart_application_3.dart'
//  as dart_application_3;
import 'dart:io' show stdin;

void main() {
  int? numb;

  while (numb != 0) {
    int numb = int.parse(stdin.readLineSync()!);
    if (numb < 3) {
      print(true);
    } else if (numb % 2 == 0) {
      print(false);
    } else if (numb % 2 != 0) {
      print(true);
    }
  }
}
